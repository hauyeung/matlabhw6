function nums = large_elements(X)
nums = [];
if ~isscalar(X)
for i=1:size(X,1)
    for j=1:size(X,2)
        if i+ j < X(i,j)
            nums = [nums; [i j]];
        end
    end
end
else
    nums = [];
end
end
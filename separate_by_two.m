function [even, odd] = separate_by_two(A)
odd = A(mod(A,2) == 1);
even = A(mod(A,2) == 0);
if iscolumn(odd)
    odd = odd'
end
if iscolumn(even)
    even = even'
end
end
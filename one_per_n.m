function n = one_per_n(x)
i = 1;
sum = 1;
while sum < x
    i = i+1;
    sum  = sum+  1/i;
    if i > 10000        
        break;
    end
end
if i > 10000
    n = -1;
else
    n = i;
end
end
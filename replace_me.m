function r = replace_me(v,a,b,c)
if nargin == 4
for i = 1:length(v)
    if v(i) == a        
        t = [v(1:i-1),b,c,v(i+1:end)];
        v = t;
    end
end
elseif nargin == 3
    for i = 1:length(v)        
    if v(i) == a && a ~= b               
        t = [v(1:i-1),b,b,v(i+1:end)];
        v = t;   
    elseif v(i) == a && a == b  
        t = [v(1:i),b,v(i+1:end)];
        v = t;   
        break
    end
    end
elseif nargin == 2
    for i = 1:length(v)
    if v(i) == a          
        t = [v(1:i-1),0,0,v(i+1:end)];
        v = t;   
    end
    end
end
r = v;
end
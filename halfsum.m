function s  = halfsum(A)
s=0;
if ~iscolumn(A)
s = sum(sum(triu(A)));
else
    s = A(1,1);
end
end
function s = square_wave(n)
t = linspace(0,4*pi,1001);
s = sin(t);
for k=2:n
    s = s + sin((2*k-1)*t)/(2*k-1);
end
end
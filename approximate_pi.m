function [p, k] = approximate_pi(delta)
k = 1;
sum = sqrt(12);
while abs(pi - sum) >= delta    
    sum  = sum + sqrt(12)*(((-3)^(-k)) /(2*k+1));    
    k = k+1;
end
p = sum;
k = k-1;
end
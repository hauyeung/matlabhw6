function d = neighbor(v)
if isvector(v) && length(v) > 1
    for i = 1 : length(v)-1
        d(i) = abs(v(i) - v(i+1));
    end    
else
    d = [];
end
end